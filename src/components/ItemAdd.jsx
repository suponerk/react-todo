import React, { Component } from "react"
import "./ItemAdd.css"

import Emoji from "./Emoji"

class ItemAdd extends Component {
  constructor(props) {
    super(props)
    this.state = {
      text: ""
    }
  }

  onChange = event => {
    //console.log("onChange event triggered: ", event.target.value)
    this.setState({
      text: event.target.value
    })
  }

  onSubmit = event => {
    //console.log("Form text: ", this.state.text)
    event.preventDefault()
    const regexp = /[A-zА-я]/
    const valid = this.state.text.match(regexp)
    const returned = valid ? this.state.text : ""

    return this.props.addItem(returned)
  }

  render() {
    return (
      <form className="item-add" onSubmit={this.onSubmit}>
        <input
          type="text"
          placeholder="type your text ..."
          onChange={this.onChange}
        />
        <button type="submit">
          <Emoji symbol="&#43;" label="+" />
        </button>
      </form>
    )
  }
}

export default ItemAdd
