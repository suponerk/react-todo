/**************************************************
 * Shows Emoji in React from it`s code
 *
 * Props:
 *  label - alternative text
 *  symbol - symbol in html format
 *
 * Usage:
 *   <Emoji label="x" symbol="&#215;" />
 *
 * Author: suponerk (suponerk@gmail.com) *
 * **************************************************/

import React from "react"

const Emoji = props => {
  return (
    <span
      className="emoji"
      role="img"
      aria-label={props.label ? props.label : ""}
      // Let`s hide element in screen readers or similar devices
      // if props.label is not specified
      aria-hidden={props.label ? "false" : "true"}
    >
      {props.symbol}
    </span>
  )
}

export default Emoji
