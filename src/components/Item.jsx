import React from "react"
import "./Item.css"

import Emoji from "./Emoji"

const style = {
  checked: {
    color: "lightgrey",
    textDecoration: "line-through",
    fontStyle: "italic"
  },
  unChecked: {
    color: "black",
    textDecoration: "none",
    fontStyle: "normal"
  }
}

const Item = props => {
  return (
    <div className="item">
      <input
        type="checkbox"
        checked={props.completed}
        onChange={() => props.toggleItem(props.itemId)}
      />
      <p style={props.completed ? style.checked : style.unChecked}>
        {props.text}
      </p>
      <button onClick={() => props.deleteItem(props.itemId)}>
        <Emoji symbol="&#215;" label="x" />
      </button>
    </div>
  )
}

export default Item
