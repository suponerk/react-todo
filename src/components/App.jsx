import React, { Component } from "react"
import Item from "./Item"
import ItemAdd from "./ItemAdd"

import "./App.css"

class App extends Component {
  constructor() {
    super()
    this.state = {
      todos: []
    }
    /***************************************************************
     * Regular function needs to bind its context to class context
     * to use this.state in operations inside it.
     * Arrow functions do not need to call .bind(this)
     ***************************************************************/
  }

  handleItemToggle = id => {
    const onStateChange = prevState => {
      const newState = prevState.todos.map(item => {
        if (item.id === id) item.completed = !item.completed
        return item
      })
      return newState
    }
    this.setState(onStateChange)
  }

  handleItemDelete = id => {
    this.setState({
      todos: this.state.todos.filter(item => item.id !== id)
    })
  }

  handleItemAdd = text => {
    if (text !== "") {
      this.setState({
        todos: this.state.todos.concat([
          {
            id: Date.now(),
            text,
            completed: false
          }
        ])
      })
    }
  }

  componentDidUpdate() {
    localStorage.todos = JSON.stringify(this.state.todos)
  }

  componentDidMount() {
    if (localStorage.getItem("todos")) {
      this.setState({
        todos: JSON.parse(localStorage.getItem("todos"))
      })
    }
  }

  render() {
    // We need to convert Object data to formatted array of <TodoItem>
    const itemList = this.state.todos.map(item => {
      return (
        <Item
          key={item.id}
          itemId={item.id}
          text={item.text}
          completed={item.completed}
          //callback functions
          toggleItem={this.handleItemToggle}
          deleteItem={this.handleItemDelete}
        />
      )
    })
    return (
      <div className="item-list">
        <ItemAdd key={this.state.todos.id} addItem={this.handleItemAdd} />
        {itemList}
      </div>
    )
  }
}

export default App
